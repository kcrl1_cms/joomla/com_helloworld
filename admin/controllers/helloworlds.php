<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorlds Controller
 *
 * @since  1.9.1
 */
class HelloWorldControllerHelloWorlds extends JControllerAdmin
{


    /**

    public function delete(){
        $input = JFactory::getApplication()->input;

        $cid = $input->get("cid",array(),"array");

        // get boxchecked that represente the number of checked items
        //	"boxchecked": INT,
        $boxchecked = $input->get("boxchecked","","INT");


        // get the  model to send parameter
        $model = $this->getModel("helloworld","helloworldmodel");

        // delete selected cids
        $model->delete($cid);

        //$msg = implode("|",$cid) . JText::_('COM_HELLOWORLD_HELLOWORLD_RECORD_DELETED');
        $msg = $boxchecked . JText::_('COM_HELLOWORLD_HELLOWORLD_RECORD_DELETED');
        // set message to display
        $this->setMessage($msg);
        $this->setRedirect(JRoute::_("index.php?option=com_helloworld&view=helloworlds"));

    }
     */

    /**
     * Proxy for getModel.
     *
     * @param   string  $name    The model name. Optional.
     * @param   string  $prefix  The class prefix. Optional.
     * @param   array   $config  Configuration array for model. Optional.
     *
     * @return  object  The model.
     *
     * @since   1.6
     */
    public function getModel($name = 'HelloWorld', $prefix = 'HelloWorldModel', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }
}